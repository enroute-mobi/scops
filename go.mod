module bitbucket.org/enroute-mobi/scops

go 1.12

require (
	bitbucket.org/enroute-mobi/ara-external-models v0.0.9
	github.com/DATA-DOG/go-sqlmock v1.4.1 // indirect
	github.com/gocraft/dbr v0.0.0-20190626032649-cb950044475e
	github.com/golang/protobuf v1.5.2
	github.com/jmoiron/sqlx v1.3.4 // indirect
	github.com/jonboulle/clockwork v0.2.2
	github.com/lib/pq v1.3.0 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/stretchr/testify v1.5.1 // indirect
)
