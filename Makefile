run:
	rm -rf plugin/ &&\
	mkdir plugin &&\
	cd plugin &&\
	git clone git@bitbucket.org:enroute-mobi/$(PLUGIN).git &&\
	cd $(PLUGIN) &&\
	go build -buildmode=plugin &&\
	cd ../.. &&\
	go run scops.go -plugin plugin/$(PLUGIN)/$(PLUGIN).so -debug

runlocal:
	rm -rf plugin/ &&\
	mkdir plugin &&\
	cd plugin &&\
	cp -r ../../scops-ineo . &&\
	cd scops-ineo &&\
	go build -buildmode=plugin -o scops-ineo.so scops_ineo.go &&\
	cd ../.. &&\
	go run scops.go -plugin plugin/scops-ineo/scops-ineo.so -debug
